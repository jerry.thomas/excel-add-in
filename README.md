# Excel Add-In Starter

A simple starter project for creating an Excel Add-In following the tutorial [here](https://docs.microsoft.com/en-us/office/dev/add-ins/tutorials/excel-tutorial).

## Changes

- Added TailwindCSS for layout
- Switched to a list based navigation
- Changed the task pane title
- Added dropdown to perform actions

## Gettig started

Clone and start the app. A temporary excel file will be launched. Click on the "TaskPane" icon in the top right to view the task pane.

```bash
git clone git@gitlab.com:jerry.thomas/excel-add-in.git
cd excel-add-in
pnpm i
pnpm build
pnpm start
```
